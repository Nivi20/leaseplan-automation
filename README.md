**#LeasePlan assignment using WebdriverIO and Cucumber BDD**

This repository contains a collection of _sample webdriverIO-v6(Selenium - Node.js/JavaScript)_ project and libraries that demonstrate how to use the tool and develop automation script using the Cucumber BDD framework. 

It uses the _chromedriver NPM package_ that wraps the ChromeDriver for you. This service does not require a Selenium server, but uses ChromeDriver to communicate with the browser directly.It is used  for end to end testing. It generates html report using _Allure_.

**#Installation**

**#Easy setup by Cloning the project**

1. Open new window in visual studio code.
2. Create a folder in your file explorer where you want to clone the repository.
3. Clone project from given link of gitlab.
4. Configure it in your folder.
5. cloning starts(it may take few minutes)
6. click on open repository.
7. Give command npm run test in bash command line.

If the above steps/cloning do not work then kindly install the dev dependencies as mentioned in package.json file 
OR follow the steps as described below.


To install this project in your system, you will need _Node.JS(v), NPM packages and JDK_

_Pre-requisites_


**Node.JS**:-Install from the site - https://nodejs.org/en/ take the latest version based on your Operating system. Please be sure that NodeJs is installed globally. To use webdriverio we need to install _Node.Js _since webdriverio is a _JavaScript_ binding.I have used node(v14.15.5)

**JAVA**: Install JAVA(v1.8) and make sure JAVA_HOME is set properly in your system environment variable.You can download _java_ from the site- https://www.java.com/en/

**NPM**:- Install _Node package modules_ in your system.It will be automatically installed when you will install _Node.JS_. I have used NPM(v6.14.11)

**Visual Studio Code**:-You need to install _Visual studio code_ for writing our scripts.You can install _visual studio code_ from the site- https://code.visualstudio.com/

**Cucumber Framework**: It is required to design BDD test cases.It can be installed from command line as described below.

**chai-assertion** : It is used in assertion. It can be installed from command line.

**Allure for reporting** : It is used for HTML reporting. It can be installed from command line.

**#WebdriverIO**
To run your test you must have selenium server up and running to execute any webdriverIO tests, or it will fail fast with an error. To start selenium automatically it has been added as part of services: ['selenium-standalone']  in the *.conf.js..!.

**#Steps for Installation**

1. Download this project and Configure the folder in Visual studio.
2. Open Terminal and type $ npm init to create package.json file that will contain all your dependencies.
3. Select your package name(all letters should be in lowercase),give description, author name and press enter and type yes to create the package for json file which contains all the information about the project.
4. To install webdriverio type the command $ npm install webdriverio --save-dev and click enter. Now webdriverio(v7.0.8) has been installed in our machine
5. To install cucumber type the command $ npm install cucumber --save-dev and click enter.
6. Install wdio-selenium-standalone-service by command $ npm install wdio-selenium-standalone-service --save-dev.
7. Install wdio/cli by command $ mpm i --save-dev @wdio/cli
8. To install allure reports type the command npm install @wdio/allure-reporter --save-dev, configure it in your wdio.conf.js file under reporters section and install allure command line by using the command [$ npm i allure-commandline] and to create an allure report type allure generate [allure_output_dir] && allure open
9. Now create config.file by command ./node_modules/.bin/wdio config and answer all queries like
   where should your tests be launched? local machine
   where is automation backend? local machine
   Which framewrork would you like to work ? cucumber
   Shall I create framework adapter for you? yes
   Do you want to run webdriverio sync or async? sync
   Do you want to add a service to your test setup? selenium-standalone
   Which reporter should be chosen? spec or allure
   Level of logging? warn

   and click enter to install all dev dependencies in package.json file.


**#Run your test scripts**

To run your test scripts, you can type syntax  "./node_modules/.bin/wdio wdio.conf.js" in bash command line.
or
You can configure test syntax in scripts within package.json file and can give command "npm run test" in bash command line to execute the scripts.

**#Steps to add a new testcase**

create new feature file or add scenario in existing feature files

 1. To add new feature, Create a new feature file in folder .\features. The name of file should be in small letter with extension .feature
 2. Write scenario outline, When and Then statements. If some test case has to be executed with different set of test data then send test as parameters as shown in below example.
 3. First line should contain name of fields, from second line, start inserting test data.
 
  Scenario Outline: As a user, clicking on electric cars should open all electric cars

   When I click on link <typeOfCarDisplayed> cars
   Then It should display atleast <NoofCarsDisplayed> cars

    Examples:
        |typeOfCarDisplayed| |NoofCarsDisplayed|
        |"Electric"| |"200"|

There are 3 step definition files in folder .\features\step-definitions. (Given.js, when.js,then.js)

For each steps described in feature file, check if the step definitions are present in these file. If step definitions are not present, it should be added here.

In step definition file, 
 a) import the module of page class file.
 b) create a object of the page class
 c) call the methods to interact with the elements on the page.
 
In folder pageobjects, in the class file define methods to interact with elements of the page.

Example :
To add new  scenario 
Scenario Outline: When a user filters electric cars and select option : Popular filters - Best deals then only electric cars with best deals should be displayed.
 
Perform following steps : 
 
Create new feature file bestdeals.feature with following text in it or add test scenarios in existing feature file which ever applicable.

  *Feature: Test filters for electric cars
       
    As a user on the LeasePlan Business page
    I  navigate to the page Showroom
    Because I want to select electric car with best deals.*

    Background: Flow till Business Showroom page
    Given I navigate to Business Showroom page
    When I click on accept cookies

    Scenario Outline: When a user filters electric cars and select option : Popular filters - Best deals then only electric cars   with best deals should be displayed.

    When I click on link <typeOfCarDisplayed> cars
    And I select <TypeOfoption> from popular filters
    Then It should display atleast <NoofCarsDisplayed> cars

    Examples:
        |typeOfCarDisplayed| |NoofCarsDisplayed| |Option|
        |"Electric"| |"200"| |"Best Deals"|


Step definition for step "When I click on link <typeOfCarDisplayed> cars" is already present in current step definitions.
Step definition for step " And I select <TypeOfoption> from popular filters" are not present in the step definition. Add the step definition of this step in file when.js
Step definition for step  Then It should display atleast <NoofCarsDisplayed> cars is already present in current step definitions.

Go to file pageobject\ShowroomPage.js
a) Write get method to locate the elements - button with Popular filters. It can be interacted in similar manner as of button with label "Monthly price" get popularFilters();
b) Write method to click on this button. clickOnPopularFilters();
c) write get method to locate check box -  Best deals. get bestDeals();
d) Write method to select the check box - Best deals. selectBestDeals();

Go to step definition page and update when.js file for step "And I select <TypeOfoption> from popular filters"
{
	ShowroomPage.clickOnPopularFilters();
	ShowroomPage.selectBestDeals();
}

if more assertions are required, then add steps accordingly.
 
