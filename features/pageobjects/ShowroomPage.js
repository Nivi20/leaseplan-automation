const { expect } = require('chai');
const Page = require('./page');

/**
 * sub page containing specific selectors and methods for a specific page
 */
class ShowroomPage extends Page {
    /**
     * define selectors using getter methods
     */
     
    get acceptCookies (){
        return $('[class="optanon-allow-all accept-cookies-button"]')
    }
    get electricCars(){
        return $('a[data-e2e-id = "/en-be/business/showroom/?fuelTypes=electric"]')
    }
    get hybridCars(){
        return $('a[data-e2e-id = "/en-be/business/showroom/?fuelTypes=hybrid"]')
    }
    get totalNumberOfCars(){ 
        return $$('span[class="translation-label"][data-key ="features.showroom.toChooseFrom"]')
    }

    get vehicleGrid(){
        return $$('[data-component = "VehicleGrid"]')
    }

    
    get allButtons(){
        return $$('button')
    }

    get resetButton(){
        return $('button[data-key="resetFilters"]').$('svg')
    }

    get saveOption() {
        return $('[class="sc-bYEvPH WidthContainedButton-sc-1an5ad7 gtczwt cVMtBp"]');
    }
    get minMonthlyPriceSlider(){
        return $('div[class="rc-slider-handle rc-slider-handle-1"]');
    }
    get maxMonthlyPriceSlider() {
        return $('div[class="rc-slider-handle rc-slider-handle-2"]');
    } 

    /* Add Get for other elements here */
        // get ClickOnPopularFilters(){return $('')}
        // get selectBestDeals
        

    /* Create function to find elements using another elements */


    // Methods to extract data from vehicle grid
    getVehicleTopText(v1){
        return v1.$('div[data-component="VehicleDescription"]').$('div[data-component="TopText"]')
       }
   
    getVehicleDescriptionMainText(v1){
        return v1.$('h2')
    }
   
    getVehicleDescriptionSubText(v1){
        return v1.$('div[data-component="Text')
    }

    getvehicleDescription(vcard){
        return vcard.$('div[data-component="VehicleDescription"]')
    }
   
    getVehiclePrice(v1){
           return v1.$('span[data-component="Heading"]').$('span[data-component="LocalizedPrice"]')
       }

    // Function to click on accept cookies. It can move to other page object file. 
    // Keeping in this page as there is only one page to automate.
    clickOnAcceptCookies(){
        browser.pause(1000);
        if(this.acceptCookies.isClickable()) {
        this.acceptCookies.click({ force: true });
    //    console.log("User accepted cookies");
        }
    }

    /* Methods to interact with Web elements on the page */

    // Clicks on link : Electric Cars
    clickOnElectricars(){
       
        this.electricCars.waitForClickable({timeout:5000});
        this.electricCars.click({ force: true });
    //    console.log("Clicked on electric car")
        
    }
     // Clicks on link : Hybrid Cars
    clickOnHybridCars(){
        
        this.hybridCars.waitForClickable({timeout:5000});
        this.hybridCars.click({ force: true });
     //   console.log("Clicked on hybrid car")
                
    }
       
    
    clickOnMonthlyPrice() {
        const but = this.allButtons;
        browser.pause(2000);
        but.forEach((b,index,$list) => {
            if(b.getText().includes('Monthly Price')){
            //    console.log(this.allButtons[index].$('svg'));
                this.allButtons[index].$('svg').waitForClickable({timeout:5000});
                this.allButtons[index].$('svg').click({force:true});
            }
         
        })
        
    }

    setMonthlyPriceRange(min,max){

        var minPercentage = ((min-200)/3660*100).toFixed(5);
        var maxPercentage = ((max-200)/3660*100).toFixed(5);
        var minStyleattr = 'left: ';
        minStyleattr = minStyleattr.concat(minPercentage, '%')
        var maxStyleattr = 'left: ';
        maxStyleattr =  maxStyleattr.concat(maxPercentage, '%')
       /* Remove this comment to check the calculation
            console.log(minPercentage);
            console.log(maxPercentage); 
        */
        this.minMonthlyPriceSlider.waitForDisplayed();
        browser.execute((minStyleattr,maxStyleattr) => {
            document.querySelector('.rc-slider-handle.rc-slider-handle-1')
                .setAttribute('style', minStyleattr)
                   
            document.querySelector('.rc-slider-handle.rc-slider-handle-2')
                .setAttribute('style', maxStyleattr);
            

        },minStyleattr,maxStyleattr)
        this.minMonthlyPriceSlider.dragAndDrop({x:0,y:1});
        this.maxMonthlyPriceSlider.dragAndDrop({x:0,y:1});

        browser.pause(5000);
        }

        clickonSave(){
            this.saveOption.click()
      }

        clickOnResetButton(){
            this.resetButton.waitForClickable({timeout:5000});
            this.resetButton.click()
      }
    
    
      // Displays to total number available based on filter
        displayAvailableNrOfCars(){
            this.totalNumberOfCars[0].waitForDisplayed();
            return parseInt(this.totalNumberOfCars[0].getText().replace(' to choose from',''));
        }

    // Displays the vehicle card sent in the method paramter.

        displayVehicleDetails(vg){
            console.log(this.getVehicleTopText(vg).getText());
            console.log(this.getVehicleDescriptionMainText(vg).getText());
            console.log(this.getVehicleDescriptionSubText(vg).getText());
            console.log(this.getVehiclePrice(vg).getText());
        }   
    
    // This function will return all the cars with same main text.
        getVehiclesDetailsOnMaintext(vehicleName){
            const vg = this.vehicleGrid;
            const vehicleDescription = [];
            vehicleDescription.push(['count',0]);
            var i = 0;
            vg.forEach((v1,index,$list) => {
        /*  Remove this comment, if you want to print all the cars which are present in the displayed list on web.
            console.log(this.getVehicleDescriptionMainText(v1).getText());*/
                if(this.getVehicleDescriptionMainText(v1).getText().includes(vehicleName))
                {
                    //console.log("Checking vehicle grid now on main text")
                    vehicleDescription.push(['main-text',this.getVehicleDescriptionMainText(v1).getText()]);
                    vehicleDescription.push(['price',this.getVehiclePrice(v1).getText()]);
                    vehicleDescription[0][1] = ++i;
                }
            
            })
            return vehicleDescription;
        }

    // This function will return all the cars with same main text and sub text
        getVehiclesDetails(vehicleName, vehicleSubText){
            const vg = this.vehicleGrid;
            const vehicleDescription = [];
            vehicleDescription.push(['count',0]);
            var i = 0;
            vg.forEach((v1,index,$list) => {
            // Remove this comment, if you want to print all the cars which are present in the displayed list on web.
            //    console.log(this.getVehicleDescriptionMainText(v1).getText());
                if(this.getVehicleDescriptionMainText(v1).getText().includes(vehicleName)) {
                    if(this.getVehicleDescriptionSubText(v1).getText().includes(vehicleSubText)){
                        console.log("Checking vehicle grid now on sub text")
                        vehicleDescription.push(['main-text',this.getVehicleDescriptionMainText(v1).getText()]);
                        vehicleDescription.push(['sub-text',this.getVehicleDescriptionSubText(v1).getText()]);
                        vehicleDescription.push(['price',this.getVehiclePrice(v1).getText()]);
                        vehicleDescription[0][1] = ++i;
                    }
                }
            
            })
            return vehicleDescription;

        }

    /**
     * overwrite specifc options to adapt it to page object
     */
    open () {
        return super.open('login');
    }
}

module.exports = new ShowroomPage();
