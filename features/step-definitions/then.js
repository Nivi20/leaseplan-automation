const { Then} = require('@cucumber/cucumber');
const { assert,expect } = require('chai');

const ShowroomPage = require('../pageobjects/ShowroomPage');

Then(/^It should display atleast "([^"]*)" cars$/, (NoofCarsDisplayed) => {
    
    expect (ShowroomPage.displayAvailableNrOfCars())
       .to.be.above(parseInt(NoofCarsDisplayed));
        
});

Then(/^Car with "([^"]*)" and "([^"]*)" should be present in search list$/, (Brand,Model) => {
    
    const vehicleDetails = ShowroomPage.getVehiclesDetails(Brand,Model);
     console.log(vehicleDetails);
     console.log(ShowroomPage.displayAvailableNrOfCars());
     expect (parseInt(vehicleDetails[0][1])).to.be.least(1)
 });

 Then(/^Car with "([^"]*)" and "([^"]*)" should not be present in search list$/, (Brand, Model) => {
    const vehicleDetails = ShowroomPage.getVehiclesDetails(Brand,Model);
    console.log(vehicleDetails);
    console.log(ShowroomPage.displayAvailableNrOfCars());
    expect (parseInt(vehicleDetails[0][1])).to.be.eq(0);
 })

 Then(/^Car with "([^"]*)" should be present in search list$/, (Brand) => {
    const vehicleDetails = ShowroomPage.getVehiclesDetailsOnMaintext(Brand);
    console.log(vehicleDetails);
    console.log(ShowroomPage.displayAvailableNrOfCars());
    expect (parseInt(vehicleDetails[0][1])).to.be.least(1);
    
 })



Then(/^All filters should be reset$/, () => {
    
    expect (ShowroomPage.displayAvailableNrOfCars())
        .to.be.above(5000);
   
         
 });


