const { When} = require('@cucumber/cucumber');
const { assert,expect } = require('chai');

const ShowroomPage = require('../pageobjects/ShowroomPage');

When(/^I click on accept cookies$/, () => {
    ShowroomPage.clickOnAcceptCookies();
       
});

When(/^I click on link "([^"]*)" cars$/, (typeOfCarDisplayed) => {
    if (typeOfCarDisplayed == 'Electric') {
    ShowroomPage.clickOnElectricars();
    }
    else if (typeOfCarDisplayed == 'Hybrid') {

    ShowroomPage.clickOnHybridCars();
    }
    else {
        console.log("User has not selected valid car type")
    }
});

When(/^Set price range "([^"]*)" to "([^"]*)"$/, (minRange,maxRange) => {
    
    ShowroomPage.clickOnMonthlyPrice();
    ShowroomPage.setMonthlyPriceRange(parseInt(minRange),parseInt(maxRange));
    ShowroomPage.clickonSave();
    
});

When(/^I click on reset button$/, () => {
     
    ShowroomPage.clickOnResetButton();
});


