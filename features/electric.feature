Feature: Test filters for electric cars
    As a user on the LeasePlan Business page
    I  navigate to the page
    Because I want to click and choose different car types

Background: Flow till Business Showroom page
Given I navigate to Business Showroom page
When I click on accept cookies

  Scenario Outline: As a user, clicking on electric cars should open all electric cars

    #Given I navigate to Business Showroom page
    #When I click on accept cookies
   When I click on link <typeOfCarDisplayed> cars
   Then It should display atleast <NoofCarsDisplayed> cars

    Examples:
        |typeOfCarDisplayed| |NoofCarsDisplayed|
        |"Electric"| |"200"|
  
  Scenario Outline: When a user clicks on electric cars and sets a price range, cars of that price range should be displayed
    When I click on link <typeOfCarDisplayed> cars
    And Set price range <minRange> to <maxRange>
    Then It should display atleast <NoofCarsDisplayed> cars

    Examples:
        |typeOfCarDisplayed| |NoofCarsDisplayed| |minRange| |maxRange|
        |"Electric"| |"20"| |"300"| |"3000"|


 Scenario Outline: When a user clicks on electric cars and set a price range then these cars should be displayed
    When I click on link <typeOfCarDisplayed> cars
    And Set price range <minRange> to <maxRange>
    Then Car with <Brand> and <Model> should be present in search list

    Examples:
        |typeOfCarDisplayed| |minRange| |maxRange| |Brand| |Model|
        |"Electric"| |"300"| |"3000"| |"Polestar 2"| |"78 KWH DUAL MOTOR LAUNCH"|
        |"Electric"| |"300"| |"3000"| |"Volkswagen Id.3"| |"58 KWH PRO LIFE"|

 Scenario Outline: When a user clicks on reset button, all filters should be reset
    When I click on link <typeOfCarDisplayed> cars
    And  Set price range <minRange> to <maxRange>
    When I click on reset button
    Then All filters should be reset

    Examples:
        |typeOfCarDisplayed| |minRange| |maxRange|
        |"Electric"| |"300"| |"3000"|


 Scenario Outline: When a user clicks on electric cars and set a price range then these cars should not be displayed
    When I click on link <typeOfCarDisplayed> cars
    And Set price range <minRange> to <maxRange>
    Then Car with <Brand> and <Model> should not be present in search list

   Examples:
        |typeOfCarDisplayed| |minRange| |maxRange| |Brand| |Model|
        |"Electric"| |"500"| |"3500"| |"Volkswagen Id.3"| |"58 KWH PRO LIFE"|