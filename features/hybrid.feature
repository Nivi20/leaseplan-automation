Feature: Test filters for hybrid cars
    As a user on the LeasePlan Business page
    I  navigate to the LeasePlan Business page
    Because I want to click and choose different hybrid cars

     Background:

        Given  I navigate to Business Showroom page
        When I click on accept cookies

Scenario Outline: As a user, clicking on hybrid cars should open all hybrid cars

    When I click on link <typeOfCarDisplayed> cars
    Then It should display atleast <NoofCarsDisplayed> cars

    Examples:
        |typeOfCarDisplayed| |NoofCarsDisplayed|
        |"Hybrid"| |"400"|
        
      
 Scenario Outline: When a user clicks on hybrid cars and sets a price range, cars of that price range should be displayed
    When I click on link <typeOfCarDisplayed> cars
    And Set price range <minRange> to <maxRange>
    Then It should display atleast <NoofCarsDisplayed> cars

    Examples:
        |typeOfCarDisplayed| |NoofCarsDisplayed| |minRange| |maxRange|
        |"Hybrid"| |"400"| |"300"| |"3000"|

 Scenario Outline: When a user clicks on hybrid cars and price range then these cars should be displayed
    When I click on link <typeOfCarDisplayed> cars
    And Set price range <minRange> to <maxRange>
    Then Car with <Brand> should be present in search list

    Examples:
        |typeOfCarDisplayed| |minRange| |maxRange| |Brand| 
        |"Hybrid"| |"300"| |"3000"| |"Audi Q3"| 
        |"Hybrid"| |"300"| |"3000"| |"BMW 3"| 
        |"Hybrid"| |"300"| |"3000"| |"Volvo Xc40"| 
@fast
 Scenario Outline: When a user clicks on reset button, all filters should be reset
    When I click on link <typeOfCarDisplayed> cars
    And  Set price range <minRange> to <maxRange>
    When I click on reset button
    Then All filters should be reset

    Examples:
        |typeOfCarDisplayed| |minRange| |maxRange|
        |"Hybrid"| |"300"| |"3000"|